class AddProfilesAssocationToUser < ActiveRecord::Migration
  def change
    add_column :profiles, :user_id, :integer
    add_index 'profiles', ['user_id'], :name => 'index_user_id'
  end
end
