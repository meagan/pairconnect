class AddFieldsToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :languages, :text
    add_column :profiles, :skill_level, :string
    add_column :profiles, :best_time, :string
    add_column :profiles, :goal, :text
  end
end
