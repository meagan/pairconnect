class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :url
      t.text :description
      t.string :language
      t.string :source_code_url
      t.text :goals
      t.text :requirements

      t.timestamps
    end
  end
end
