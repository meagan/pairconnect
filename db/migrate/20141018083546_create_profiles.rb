class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.text :bio
      t.string :twitter_handle
      t.string :github_username
      t.string :website

      t.timestamps
    end
  end
end
