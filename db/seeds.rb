# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

@project = Project.create({description: "Dad Jokes as a service", language: "Ruby", name: "Dad Jokes for All", requirements: "Ruby Environment"})
@profile = Profile.create(best_time: "Early Morning", bio: "I love to write software", goal: "To learn how to write better tests", languages: "Ruby, Python", skill_level: "novice")

["alice", "bob", "carol", "danny", "eliza", "fred", "ginny", "henry"].each do |name|
  user_test = User.create({activation_state: "active", password: "secretpw", password_confirmation: "secretpw", email: "#{name}@example.com", username: "#{name}"})
  user_test.activate!
  user = User.find_by_slug("#{name}")
  user.projects << @project
  user.profile = @profile
  user.save!
end



