module UsersHelper
  def skill_level(level)
    skill_level_str = ""
    if level == "newbie"
      skill_level_str += "Total Newbie"
    elsif level == "novice"
      skill_level_str += "Novice"
    elsif level == "advanced_beginner"
      skill_level_str += "Advanced Beginner"
    elsif level == "intermediate"
      skill_level_str += "Intermediate"
    elsif level == "advanced"
      skill_level_str += "advanced"
    end
    return skill_level_str
  end
end
