class StaticController < ApplicationController
  before_filter :require_login, only: :contact
  layout "landing"

  def index
  end

  def about
  end

  def contact
    if params[:email] != nil
      flash[:notice] = "Your message has been sent."
      ContactMailer.contact_email(params).deliver
    end
  end

end
