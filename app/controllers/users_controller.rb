class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_filter :require_login, only: [:index, :new, :create, :activate]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @projects = @user.projects.limit(5)
    @online = User.get_current_users.include?(@user)
  end

  # GET /users/new
  def new
    if session[:user_id]
      user = User.friendly.find(session[:user_id])
      if !user.guest?
        redirect_back_or_to(:users, notice: "You're Already Logged In")
      elsif user.guest?
        @user = User.new
        @user.build_profile if @user.profile.nil?
      end
    else
      @user = User.new
      @user.build_profile if @user.profile.nil?
    end
  end

  # GET /users/1/edit
  def edit
    if !verify_current_user
      redirect_to root_url, notice: "You don't have permission to do this."
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = params[:user] ? User.new(user_params) : User.new_guest
    @user.build_profile if @user.profile.nil?

    if @user.save
      if @user.guest?
        session[:user_id] = @user.id
        redirect_to root_url, notice: "Guest Account Successfully Created."
      else
        redirect_to root_url, notice: "Check Your Email to Activate Your Account."
      end
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if verify_current_user
      if @user.update(user_params)
        flash[:notice] = "Successfully Updated!"
        render :edit
      else
        render action: 'edit'
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if verify_current_user
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to root_path, notice: "You aren't permitted to do this." }
      end
    end
  end

  def activate
    if (@user = User.load_from_activation_token(params[:id]))
      @user.activate!
      redirect_to(login_path, notice: "Used was successfully activated.")
    else
      not_authenticated
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :username, :profile_attributes => [:id, :name, :bio, :twitter_handle, :github_username, :website, :languages, :skill_level, :best_time, :goal, :_destroy], :projects_attributes => [:id, :name, :url, :description, :language, :source_code_url, :goals, :requirements, :_destroy])
  end

  def verify_current_user
    @user.id == session[:user_id]
  end
end
