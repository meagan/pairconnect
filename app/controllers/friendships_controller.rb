class FriendshipsController < ApplicationController

  def index
    @friends = current_user.friends
  end

  def new
    @friends = User.all conditions: ["id != ?", current_user.id]
  end

  def create
    invitee = User.find_by_slug(params[:user_id])
    if current_user.invite invitee
      redirect_to new_friend_path, notice: "Successfully Invited #{invitee.username}"
    else
      redirect_to new_friend_path, notice: "Sorry! You can't invite that user."
    end
  end

  def update
    inviter = User.find_by_slug(params[:id])
    if current_user.approve inviter
      redirect_to new_friend_path, notice: "Successfully confirmed #{inviter.username}"
    else
      redirect_to new_friend_path, notice: "Sorry! Could not confirm #{inviter.username}"
    end
  end

  def requests
    @pending_requests = current_user.pending_invited_by
  end

  def invites
    @pending_invites = current_user.pending_invites
  end

  def destroy
    user = User.find_by_slug(params[:id])
    if current_user.remove_friendship user
      redirect_to friends_path, notice: "Succesfully removed friend!"
    else
      redirect_to friends_path, notice: "Sorry, couldn't remove friend!"
    end
  end
end
