class User < ActiveRecord::Base
  extend FriendlyId
  include Amistad::FriendModel
  include Gravtastic
  gravtastic
  friendly_id :username, use: :slugged
  authenticates_with_sorcery!

  validates :password, length: {minimum: 8}, unless: :guest?, on: :create
  validates :password, confirmation: true, unless: :guest?, on: :create
  validates :password_confirmation, presence: true, unless: :guest?, on: :create
  validates :email, uniqueness: true, unless: :guest?
  validates :email, presence: true, unless: :guest?
  validates :username, presence: true, unless: :guest?
  validates :username, uniqueness: true, unless: :guest?

  has_one :profile, :dependent => :destroy
  has_many :projects, :dependent => :destroy
  accepts_nested_attributes_for :projects, :allow_destroy => true
  accepts_nested_attributes_for :profile, :allow_destroy => true

  def self.new_guest
    new { |u| u.guest = true }
  end

  def name
    guest ? "Guest" : username
  end

  def self.get_current_users
    config = sorcery_config
    where("#{config.last_activity_at_attribute_name} IS NOT NULL") \
    .where("#{config.last_logout_at_attribute_name} IS NULL
      OR #{config.last_activity_at_attribute_name} > #{config.last_logout_at_attribute_name}") \
    .where("#{config.last_activity_at_attribute_name} > ? ", config.activity_timeout.seconds.ago.utc.to_s(:db))
  end
end

