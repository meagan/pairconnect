# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#

$ ->

  hideAllForms = () ->
    $(".edit-profile-form").hide()
    $(".social-form").hide()
    $(".pairing-form").hide()
    $(".notification-form").hide()
    $(".cancel-account-form").hide()

  removeActiveClasses = ->
    $("li.edit-profile-link").removeClass 'active'
    $("li.edit-social-info-link").removeClass 'active'
    $("li.edit-pairing-info-link").removeClass 'active'
    $("li.edit-notification-link").removeClass 'active'
    $("li.cancel-account-link").removeClass 'active'

  $("li.edit-social-info-link").click (e) ->
    hideAllForms()
    removeActiveClasses()
    $(this).addClass 'active'
    $(".social-form").show()
    e.preventDefault()

  $("li.edit-profile-link").click (e) ->
    hideAllForms()
    removeActiveClasses()
    $(this).addClass 'active'
    $(".edit-profile-form").show()
    e.preventDefault()

  $("li.edit-pairing-info-link").click (e) ->
    hideAllForms()
    removeActiveClasses()
    $(this).addClass 'active'
    $(".pairing-form").show()
    e.preventDefault()

  $("li.edit-notification-link").click (e) ->
    hideAllForms()
    removeActiveClasses()
    $(this).addClass 'active'
    $(".notification-form").show()
    e.preventDefault()

  $("li.cancel-account-link").click (e) ->
    hideAllForms()
    removeActiveClasses()
    $(this).addClass 'active'
    $(".cancel-account-form").show()
    e.preventDefault()

  $("a.add-project").click (e) ->
    $(".no-project-text").hide()
