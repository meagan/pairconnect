# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
$ ->

  hideBoxes = () ->
    $("div.inbox-section").hide()
    $("div.sent-box-section").hide()
    $("div.trash-section").hide()
    $("div.all-three").hide()

  $("i.fa.fa-inbox").click (e) ->
    hideBoxes()
    $("div.inbox-section").show()
    e.preventDefault()

  $("i.fa.fa-paper-plane").click (e) ->
    hideBoxes()
    $("div.sent-box-section").show()
    e.preventDefault()

  $("i.fa.fa-trash").click (e) ->
    hideBoxes()
    $("div.trash-section").show()
    e.preventDefault()

