class UserMailer < ActionMailer::Base
  default from: "admin@pairconnect.me"

  def reset_password_email(user)
    @user = user
    @url  = edit_password_reset_url(user.reset_password_token)
    mail(to: user.email, subject: "Your password has been reset")
  end

  def activation_needed_email(user)
    @user = user
    @activation_token = @user.activation_token
    mail(to: @user.email, subject: "Welcome to PairConnect!")
  end

  def activation_success_email(user)
    @user = user
    mail(to: @user.email, subject: "Your Pair Connect Account is now activated.")
  end
end
